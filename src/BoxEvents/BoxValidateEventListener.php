<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 25/11/18
 * Time: 22:40
 */

namespace App\BoxEvents;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\GuardEvent;

class BoxValidateEventListener implements EventSubscriberInterface
{

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            'workflow.box_making.guard.validate' => ['guardValidate']
        ];
    }


    public function guardValidate(GuardEvent $event)
    {
        /** @var \App\Entity\Box $box */
        $box = $event->getSubject();
        $products = $box->getProducts();

        if (count($products)==0) {
            $event->setBlocked(true);
        }
    }
}