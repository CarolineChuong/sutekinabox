<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 21/11/18
 * Time: 16:19
 */

namespace App\Controller;


use App\Entity\Box;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $boxes = $this->getDoctrine()->getRepository(Box::class)->findAll();
        return $this->render('index/index.html.twig', [
            'boxes' => $boxes
        ]);
    }
}