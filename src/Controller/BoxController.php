<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 22/11/18
 * Time: 17:50
 */

namespace App\Controller;


use App\Entity\Box;
use App\Entity\Product;
use App\Form\BoxType;
use App\Repository\BoxRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\Exception\TransitionException;
use Symfony\Component\Workflow\Registry;

/**
 * @Route("/box")
 * @Security("has_role('ROLE_STAFF')")
 */
class BoxController extends AbstractController
{
    /**
     * @Route("/", name="box_index")
     * @param BoxRepository $boxRepository
     * @return Response
     */
    public function index(BoxRepository $boxRepository): Response
    {
        $boxes = $boxRepository->findAll();
        return $this->render('box/index.html.twig', [
            'boxes' => $boxes
        ]);
    }

    /**
     * @Route("/new", name="box_new", methods="GET|POST")
     * @param Request $request
     * @param Registry $workflows
     * @return Response
     */
    public function newBox(Request $request, Registry $workflows): Response
    {
        $box = new Box();
        $workflow = $workflows->get($box);
        $boxWorkflows = $workflows->all($box);


        $transitions = $workflow->getEnabledTransitions($box);

        dump($transitions);
        $form = $this->createForm(BoxType::class, $box);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $totalPrice = 0;
            /** @var Product $product */
            foreach ($box->getProducts() as $product) {
                $totalPrice += $product->getPrice();
            }
            $box->setTotalPrice($totalPrice);

            $em = $this->getDoctrine()->getManager();
            $em->persist($box);
            $em->flush();

            return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
        }

        return $this->render('box/new.html.twig', [
            'box' => $box,
            'form' => $form->createView(),
            'transitions' => $transitions
        ]);
    }

    /**
     * @Route("/{id}", name="box_show", methods="GET")
     * @param Box $box
     * @param Registry $workflows
     * @return Response
     */
    public function showBox(Box $box, Registry $workflows): Response
    {
        $workflow = $workflows->get($box);
        $allProducts = $this->getDoctrine()->getRepository(Product::class)->findAll();
        return $this->render('box/show.html.twig', [
            'box' => $box,
            'allProducts' => $allProducts
        ]);
    }

    /**
     * @Route("/{id}/edit", name="box_edit", methods="GET|POST")
     * @param Request $request
     * @param Box $box
     * @param Registry $workflows
     * @return Response
     */
    public function editBox(Request $request, Box $box, Registry $workflows): Response
    {
        $form = $this->createForm(BoxType::class, $box);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
        }

        return $this->render('box/edit.html.twig', [
            'box' => $box,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/add_product/{id_product}", name="box_add_product")
     * @param int $id
     * @param $id_product
     * @param Registry $workflows
     * @return RedirectResponse
     */
    public function addProductToBox(int $id, $id_product, Registry $workflows): RedirectResponse
    {

        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id_product);

        $box->addProduct($product);

        $totalPrice = 0;
        /** @var Product $product */
        foreach ($box->getProducts() as $product) {
            $totalPrice += $product->getPrice();
        }
        $box->setTotalPrice($totalPrice);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);

    }

    /**
     * @Route("/{id}/remove_product/{id_product}", name="box_remove_product")
     * @param int $id
     * @param $id_product
     * @return RedirectResponse
     */
    public function removeProductFromBox(int $id, $id_product): RedirectResponse
    {
        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id_product);
        $box->removeProduct($product);

        $totalPrice = 0;
        /** @var Product $product */
        foreach ($box->getProducts() as $product) {
            $totalPrice += $product->getPrice();
        }
        $box->setTotalPrice($totalPrice);

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);

    }

    /**
     * @Route("/{id}/validate_box", name="box_validate")
     * @param int $id
     * @param Registry $workflows
     * @return RedirectResponse
     */
    public function validateBox(int $id, Registry $workflows): RedirectResponse
    {
        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $workflow = $workflows->get($box);

        if ($box->getTotalPrice()>$box->getBudget()){
            $this->addFlash('error', 'Hey you have exceeded the budget! Please remove some items :)');
        } else {
            try {
                $workflow->apply($box, 'validate');
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('notice', 'This box is validated!');
            } catch (TransitionException $exception) {
                $this->addFlash('error', 'Oh no you can\'t do it');
            }
        }

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
    }

    /**
     * @Route("/{id}/ask_supplier", name="box_ask_supplier")
     * @param int $id
     * @param Registry $workflows
     * @return RedirectResponse
     */
    public function askSupplier(int $id, Registry $workflows): RedirectResponse
    {
        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $workflow = $workflows->get($box);

        try {
            $workflow->apply($box, 'ask_supplier');
            $this->addFlash('notice', 'An email has been sent to supplier(s)!');
            $this->getDoctrine()->getManager()->flush();

        } catch (TransitionException $exception) {
            $this->addFlash('error', 'Oh no you can\'t do it');
        }

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
    }

    /**
     * @Route("/{id}/reject_order", name="box_reject_order")
     * @param int $id
     * @param Registry $workflows
     * @return RedirectResponse
     */
    public function rejectOrder(int $id, Registry $workflows): RedirectResponse
    {
        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $workflow = $workflows->get($box);

        try {
            $workflow->apply($box, 'reject_order');
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Please update this box content!');
        } catch (TransitionException $exception) {
            $this->addFlash('error', 'Oh no you can\'t do it');
        }

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
    }

    /**
     * @Route("/{id}/accept_order", name="box_accept_order")
     * @param int $id
     * @param Registry $workflows
     * @return RedirectResponse
     */
    public function acceptOrder(int $id, Registry $workflows): RedirectResponse
    {
        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $workflow = $workflows->get($box);

        try {
            $workflow->apply($box, 'accept_order');
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Yay this order has been accepted!');
        } catch (TransitionException $exception) {
            $this->addFlash('error', 'Oh no you can\'t do it');
        }

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
    }

    /**
     * @Route("/{id}/manager_validate_box", name="box_manager_validate")
     * @Security("has_role('ROLE_MANAGER')")
     * @param $id
     * @param Registry $workflows
     * @return RedirectResponse
     */
    public function managerValidateBox($id, Registry $workflows): RedirectResponse
    {
        $box = $this->getDoctrine()->getRepository(Box::class)->find($id);
        $workflow = $workflows->get($box);

        try {
            $workflow->apply($box, 'manager_validation');
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'All good! Go! Go! Go!');
        } catch (TransitionException $exception) {
            $this->addFlash('error', 'Oh no you can\'t do it');
        }

        return $this->redirectToRoute('box_show', ['id' => $box->getId()]);
    }
}